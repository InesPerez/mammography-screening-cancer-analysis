import csv, os, glob
import pandas as pd

import sys
sys.path.append('./ddsm_tools/')

from parse_ddsm_metadata import make_data_set 

if __name__ == '__main__':
	cases=[]
	main_path=os.getcwd()+'/ddsm_tools/DDSM/cases/'
	for root, dirs, files in os.walk(main_path):
		
		for name in dirs:
			curr_path=os.path.join(root, name)
			if "case" in name: 
				cases.append(curr_path)
				volumes_path=curr_path+'/Volumes'
				print(volumes_path)
				try:
					os.mkdir(volumes_path, 0755 );
				except OSError:
					print 'Folder already exists.'
	print cases
	
	csv_files=[]
	for case in cases:
		try:
			print case
			make_data_set(case, out_dir=case+'/Volumes')
			csv=glob.glob(case+'/Volumes/*.csv')
			print csv
			csv_files.append(csv[0])
		except IOError:
                	print 'The file could not be decompressed\n'
	print csv_files
	
	df_from_each_file = (pd.read_csv(f, sep=',') for f in csv_files)
	df_merged   = pd.concat(df_from_each_file, ignore_index=True)
	df_merged.to_csv( "cancer.csv")
