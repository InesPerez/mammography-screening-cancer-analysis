#include <Xm/Xm.h>

#define UP            0  /* | */
#define RIGHT_UP      1  /* / */
#define RIGHT         2  /* - */
#define RIGHT_DOWN    3  /* \ */
#define DOWN          4  /* | */
#define LEFT_DOWN     5  /* / */
#define LEFT          6  /* - */
#define LEFT_UP       7  /* \ */

#define EVEN 0
#define ODD  1

#define YES                 1
#define NO                  0

#define START_FILLING       0
#define STOP_FILLING        1
#define VERTEX              2
#define TANGENT             3
#define NOP                 4
#define NOTNOP              5

#define CONTINUE            0
#define STOP                1

#define NOFILL              0
#define FILL                1
#define FILL_ON_IMAGE       2


#define ONLYCHAIN           -1

typedef struct
{
  int x;        /* x position of a point on the chain */
  int y;        /* y position of a point on the chain */
} Chain_Code;  


typedef struct
{
  Chain_Code          Points[15000];       /* # of points on the chain */
  Chain_Code          *Final_Pts;
  unsigned short int  *TempValue;
  int                 Resolution;         /* The Resolution of the image */
  int                 Total_Points;       /* # of points on the chain*/
  int                 *Output_ChainCode;
  int                 Abnormality;       /*number of abnormal of this mark*/
  char                Lesion_Type[20];   /*mass or microcalcificaiton*/
  char                Lesion_Keywd1[20];  /*title name*/
  char                Lesion_Subwd1[20]; /*shape or types*/
  char                Lesion_Keywd2[20];  /*title name*/
  char                Lesion_Subwd2[20]; /*margin or distribution*/
  int                 Assessment;        /*assessment*/ 
  int                 Subtlety;          /*subtlety*/
  int                 M_B;               /*malignant or benign*/
  char                Pathology[5];      /*certainty level*/
  int                 Total_Outline;     /*total outline of this abnormality*/
  int                 Type;              /* If more than one overlays */
  int                 Class;
} Chains;

Chains The_Code[50]; /* number of chain codes on an image */

typedef struct
{
  char               FileName[200];
  int                Total_Abnormality; /*total number of abnormality*/
 
  Chains             OVERLAY_Mark[50]; /* number of chain codes on an image */
  int                Start_x, Start_y;
  int                Shown;
} OVERLAY_TYPE;

OVERLAY_TYPE  Overlay; /* Overlay will hold the chain codes on the abn. image*/


typedef struct
{
  Widget             widget;
  unsigned char      *Image;
  unsigned short int *Image_16;
  int                Width;
  int                Height;
  int                Width_16;
  int                Height_16;
  int                Image_Position_LTop_V;
  int                Image_Position_LTop_H; 
  char               FileName[200];
  Pixmap             ABNPixmap;
} ABNIMAGE;

ABNIMAGE TemplateImage;

/*
** ID => Index for the Chain Codes
*/

int    ID, Num_ClassG;
