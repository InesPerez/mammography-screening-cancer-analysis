/*
** ics2pgm is compiled on Sunos Version.
** 
** Copyright (C) 1996  Kyong Chang, Maha Sallam, Kevin Woods, Kevin Boywer.
** This program converts ics format, which is a default format in DDSM 
** database, into 16 bit pgm format.
** Thus, users can view the images through xv or other image viewing tools.
** No warranty or maintenance is given, either expressed or implied.  
** In no event shall the author(s) be liable to you or a third party 
** for any special, incidental, consequential, or other damages, 
** arising out of the use or inability to use the program for any purpose 
** (or the loss of data), even if we have been advised of such possibilities.
**
*/

/* 
** ics2pgm.c
*/

/* Include files & Header files. */

#include   <malloc.h>
#include   <stdio.h>
#include   <string.h>
#include   <ctype.h>
#include   <stdlib.h>

#define ORDER     12

#define LCC_H     16
#define LCC_W     18

#define LMLO_H    25 
#define LMLO_W    27 

#define RCC_H     34
#define RCC_W     36

#define RMLO_H    43 
#define RMLO_W    45 

/*
** ics data file will be stored into Data[100][100]
*/

char Data[100][100];


/*
** Input: case identification number
*/

int Read_ICS_FILE(case_id)
char* case_id;
{  
  FILE *file_ptr;
  char *temp1, *ICS_FileName;
  char record[91], tempcase[100];
  int  count, size, i;
  char temp_order[15];

  ICS_FileName = (char *)malloc(200 * sizeof(char));    

  sprintf(tempcase, "%s", case_id);

  for(i = 0; i <= strlen(tempcase); i++)
    if(tempcase[i] == '_')
      tempcase[i] = '-';

  sprintf(ICS_FileName, "%s-1.ics", tempcase);

  file_ptr = fopen(ICS_FileName, "r");
  if(file_ptr == NULL)
    {
      printf("WARNING: The ICS File Does Not Exist !\n");
      return 0;
    }

  /*
  ** Open the ICS file and extract the all the information.
  ** Extract single string after the white space of each field.
  */

  count = 0;
  while(fgets(record, 90, file_ptr) != NULL)
    {
      size = strlen(record);      
      temp1 = strtok(record, " ");
      if(temp1 != NULL)
	{
	  if(count == 15)
	    {
	      if(strstr(temp1, "SE"))
		strcpy(temp_order, temp1);
	    }
	}

      if(temp1 != NULL)
	{
	  for(i = 0; i < size; i++)
	    {
	      temp1 = strtok(NULL, " ");
	      if(temp1 == NULL) 
		break;
	      strcpy(Data[count], temp1);
	      count++;
	    }
	}	
    }
  strcpy(Data[ORDER], temp_order);
  /*
  while(fgets(record, 90, file_ptr) != NULL)
    {
      size = strlen(record);
      
      temp1 = strtok(record, " ");
      if(temp1 != NULL)
	{
	  for(i = 0; i < size; i++)
	    {
	      temp1 = strtok(NULL, " ");
	      if(temp1 == NULL) 
		{
		  break;
		}		      
	      strcpy(Data[count], temp1);
	      count++;
	    }
	}	
    }
    */
  fclose(file_ptr);
  free(ICS_FileName);

  return 1;
}

/*
** Input: ICS format image(raw), image_array, image width and height
** This function will read from the disk and store the image data
** into the image array. Width and height are from Data array.
*/

int readRawImage(file_name, image_array, image_width, image_height, 
                 bytes)

char            file_name[];
unsigned char   **image_array;
int             image_width, image_height;
int             bytes;
{
  FILE    *input_file, *fopen();
  char    dummy[255];
  int     i,j;

  input_file = fopen(file_name, "r");
  if(input_file == NULL)
  {
    printf("\nERROR: file %s not found\n", file_name);
    return(1);
  }
  
  *image_array = (unsigned char *) malloc(bytes*image_width*image_height*
                                          sizeof(unsigned char)); 

  if((*image_array) == NULL)
  {
    printf("\n ERROR: out of memory in readPgmImage");
    return(1);
  }

  fread((*image_array), bytes, image_width * image_height, input_file);

  fclose(input_file);
  return(0);
}

/*
** Input : Output filename(pgm), image array, image width and height.
** This function writes out the ics format(raw) plus width and heigth 
** information, magic number, and the hightest intensity value in the image.
*/

int write16bitPgmImage(file_name, image_array, image_width, image_height)

char            file_name[];
unsigned short  *image_array;
int             image_width, image_height;
{
  FILE    *output_file, *fopen();

  output_file = fopen(file_name, "w");

  if(output_file == NULL)
  {
    printf("\nERROR: cannot open file %s\n", file_name);
    return(1);
  }

  fprintf(output_file, "%s\n", "P5");

  fprintf(output_file, "%d %d\n", image_width, image_height);

  fprintf(output_file, "%d\n\n",65535);

  fwrite(image_array, 2, image_width*image_height, output_file);
  fflush(output_file);
  fclose(output_file);

  return(0);
}

/*
** Input: input filename for jpeg uncompression.
** This function will use system call to use the jpeg by Andy C. Hung, 
*/

int Uncompress(Jpeg_Command, file_name)
char *Jpeg_Command, *file_name;
{
  
  if(fopen(file_name, "r"))
    {
      printf("%s is already on the disk!!\n\n", file_name);
    }
  else
    {      
      if(!system(Jpeg_Command))          
	{
	  printf("\t Uncompression JPEG Has Been Done ! \n");
	}
      else
	{
	  printf("\t Uncompressing JPEG Has NOT Been Done Properly !\n");
	}
    }
  return;
}

void main(argc, argv)
int  argc;
char *argv[];
{
  int view, i;
  unsigned short int *image_array;
  char file_name[200];
  char *JPEG_Flg, case_id[10];

  if((argc < 2) || (argc > 2))
    {
      printf("\n************************");
      printf("\n* USAGE: ics2pgm case# *");
      printf("\n************************");
      printf("\n Example: ics2pgm A_0782 \n\n");  
      exit(1);    
    }

  strcpy(case_id, argv[1]);

  printf("Now, ics format is being converted into pgm format....\n\n");

  Read_ICS_FILE(case_id);

  JPEG_Flg = (char *)malloc(200 * sizeof(char));    

  /*
  ** Left CC View
  */
  strcpy(JPEG_Flg, "jpeg -d -s ");

  sprintf(file_name, "%s_1.LEFT_CC.LJPEG", case_id);

  strcat(JPEG_Flg, file_name);

  strcat(file_name, ".1");

  Uncompress(JPEG_Flg, file_name);

  readRawImage(file_name, &image_array, 
	       atoi(Data[LCC_W]), atoi(Data[LCC_H]), 2);

  strcat(file_name, ".pgm");
  
  write16bitPgmImage(file_name, image_array,
		     atoi(Data[LCC_W]), atoi(Data[LCC_H]));
  free(image_array);
  free(JPEG_Flg);


  /*
  ** LEFT MLO View
  */
  strcpy(JPEG_Flg, "jpeg -d -s ");

  sprintf(file_name, "%s_1.LEFT_MLO.LJPEG", case_id);

  strcat(JPEG_Flg, file_name);

  strcat(file_name, ".1");

  Uncompress(JPEG_Flg, file_name);

  readRawImage(file_name, &image_array, 
	       atoi(Data[LMLO_W]), atoi(Data[LMLO_H]), 2);
  
  strcat(file_name, ".pgm");

  write16bitPgmImage(file_name, image_array,
		     atoi(Data[LMLO_W]), atoi(Data[LMLO_H]));
  free(image_array);  
  free(JPEG_Flg);


  /*
  ** Right CC View
  */
  strcpy(JPEG_Flg, "jpeg -d -s ");

  sprintf(file_name, "%s_1.RIGHT_CC.LJPEG", case_id);

  strcat(JPEG_Flg, file_name);

  strcat(file_name, ".1");

  Uncompress(JPEG_Flg, file_name);

  readRawImage(file_name, &image_array, 
	       atoi(Data[RCC_W]), atoi(Data[RCC_H]), 2);
  
  strcat(file_name, ".pgm");
  
  write16bitPgmImage(file_name, image_array,
		     atoi(Data[RCC_W]), atoi(Data[RCC_H]));
  free(image_array);
  free(JPEG_Flg);

  
  /*
  ** Right MLO View
  */
  strcpy(JPEG_Flg, "jpeg -d -s ");

  sprintf(file_name, "%s_1.RIGHT_MLO.LJPEG", case_id);

  strcat(JPEG_Flg, file_name);

  strcat(file_name, ".1");

  Uncompress(JPEG_Flg, file_name);

  readRawImage(file_name, &image_array, 
	       atoi(Data[RMLO_W]), atoi(Data[RMLO_H]), 2);
  
  
  strcat(file_name, ".pgm");
  
  write16bitPgmImage(file_name, image_array,
		     atoi(Data[RMLO_W]), atoi(Data[RMLO_H]));
  free(image_array);
  free(JPEG_Flg);


  return;
}



 

