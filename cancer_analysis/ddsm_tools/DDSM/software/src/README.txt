File: README.txt

The createTemplate and compareTemplate programs are known to have bugs,
and they are not being supported. If you are planning on using this code,
please note that you are doing this at your own risk.

Michael Heath
