clc,clear all,close all
% readtable('cancer.csv');
% relative_path=extract_path(subjects.od_img_path);
% mask_path=extract_path(subjects.mask_path);

subjects=dir('./ddsm_tools/DDSM/cases/cancers_2/**/*.jpg');
subjects=struct2table(subjects);

masks=string(subjects.name(contains(subjects.name,'_Mask')));
for i=1:numel(masks)
    mask=string(masks(i));
    try
    subj=extractBefore(mask,strfind(mask,'_Mask'));
    catch
    subj=extractAfter(mask,strfind(mask,'Mask_'));
    end
    
    images=subjects.folder(contains(subjects.name,subj));
    images=strcat(images,'\',subjects.name(contains(subjects.name,subj)));

    left=contains(images,'LEFT');
    cc=contains(subj,'CC');

    image=imread(string(images(~contains(images,'Mask'))));
    image_mask=imread(string(images(contains(images,'_Mask'))));

    if left
        image=flipdim(image,2);
        image_mask=flipdim(image_mask,2);
    end
    
    if cc
        [~,detected_area]=cancer_mam(image,image_mask,1);
        image_border=detected_area.dil_border{:,:};
        image(image_border)=0;
        figure;imshow(image);
    end
end

% for i=1:numel(relative_path)
%     left=contains(relative_path(i),'LEFT');
%     mlo=contains(relative_path(i),'MLO');
%     image=imread(relative_path(i));
%     mask=imread(mask_path(i));
%     if left
%         image=flipdim(image,2);
%         mask=flipdim(mask,2);
%     end
%     
%     if ~mlo
%         [~,detected_area]=cancer_mam(image,mask,1);
%         image_border=detected_area.dil_border{:,:};
%         image(image_border)=0;
% 
%         figure;imshow(image);
%     end
% end

function relative_path=extract_path(column)

origin_path=string(column);

index=table2array(cell2table(strfind(origin_path,'ddsm_tools')))-1;
relative_path=strcat('./',extractAfter(origin_path,index)) ;

end
