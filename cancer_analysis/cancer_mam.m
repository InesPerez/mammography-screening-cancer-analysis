function [border,border_max_sim]=cancer_mam(image,original_mask,show)

if nargin<3
    show=0;
end
original_mask=logical(original_mask);
original_mask=~imfill(original_mask,8);

image_mask=image;
image_mask(~original_mask)=255;

%% Espacios de color
aux=figure;
rgb=gray2rgb(image);
close(aux);
% lab=rgb2lab(rgb);
% ntsc=rgb2ntsc(rgb);
% ycbcr=rgb2ycbcr(rgb);
xyz=rgb2xyz(rgb);

% figure
% subplot(2,3,1),imshow(rgb)
% title('RGB')
% subplot(2,3,2),imshow(lab)
% title('LAB')
% subplot(2,3,3),imshow(ntsc)
% title('NTSC')
% subplot(2,3,4),imshow(ycbcr)
% title('YCBCR')
% subplot(2,3,5),imshow(xyz)
% title('XYZ')

if show
    figure
    subplot(1,2,1);imshow(image_mask)
    subplot(1,2,2);imshow(image)
end

% [lum,R,G,B]=channels(rgb);
% sgtitle("RGB");
% 
% channels(lab,1);
% sgtitle("LAB");
% channels(ntsc,1);
% sgtitle("NTSC");
% channels(ycbcr,1);
% sgtitle("YCBCR");
[~,~,~,Z]=channels(xyz);
Z=1-Z;
% sgtitle("XYZ");

[border,border_max_sim]=borders(Z,0.5,0.8,0.01,original_mask,show);
end

function [image_gray,R,G,B]=channels(image,show)
if nargin<2
    show=0;
end
% RGB Channels
image_gray=rgb2gray(image);
R=image(:,:,1);
G=image(:,:,2);
B=image(:,:,3);

if show
    figure
    subplot(2,2,1),imshow(image_gray)
    title('Luminance')
    subplot(2,2,2),imshow(R)
    title('R')
    subplot(2,2,3),imshow(G)
    title('G')
    subplot(2,2,4),imshow(B)
    title('B')
end

end

function histogram_graphic(image)
% Histogramas
figure
[counts_rgb,binlocations_rgb]=imhist(image);
histogram(counts_rgb,binlocations_rgb);
title('Luminancia')
end

function BW=thresholding(image,umbral,show)
    if nargin<3
        show=0;
    end
    %% Umbralización
    BW=imbinarize(image,umbral);
    if show
        figure
        subplot(1,2,1);imshow(image);
        title('Original image')
        subplot(1,2,2);imshow(BW);
        title('Brightning thresholding')
    end
end

function section=extract_area(mask,option,show)
    if nargin<3
        show=0;
    end

    %Regions
    CC = bwconncomp(mask, 8);
    S = regionprops(CC, 'Area');
    L = labelmatrix(CC);
    if option{1}==1
        max_area = ismember(L, find([S.Area] == max([S.Area])));
        section=max_area;
    elseif option{1}==2
        reference_region=option{2};
        area_sorted=sort([S.Area],'descend');
        common=0;
        n=0;
        try
            while sum(common)==0
                n=n+1;
                container_area = ismember(L, find([S.Area] == area_sorted(n)));
                
                common=container_area & reference_region;
            end
        catch
                fprintf('Cancer did not found.\n')
        end
        section=container_area;
    end

    if show
        figure
        imshow(section)
    end
end

function [images_umbral,max_sim]=borders(image,ini,fin,step,mask,show)
    if nargin<5
        show=0;
    end

    %% Brighter region
    mascara=thresholding(image,fin);

    %Removing label
    label_x=[0 0 500 500];
    label_y=[0 500 500 0];
    without_label=poly2mask(label_x,label_y,size(mascara,1),size(mascara,2));
    mascara(without_label)=0;

    %Removing image border
    label_x=[0   size(mascara,2)-100 size(mascara,2)-60 size(mascara,2)-60  0];
    label_y=[100 100                 400                size(mascara,1)-200 size(mascara,1)-200];
    without_lateral=~poly2mask(label_x,label_y,size(mascara,1),size(mascara,2));
    mascara(without_lateral)=0;
%     figure; imshow(mascara);

    brigther_area = extract_area(mascara,{1});    

    images_umbral=table;

    N=(fin-ini)/step;
    for i=0:N
        mascara=thresholding(image,ini+step*i);

        %Removing label
        label_x=[0 0 500 500];
        label_y=[0 500 500 0];
        without_label=poly2mask(label_x,label_y,size(mascara,1),size(mascara,2));
        mascara(without_label)=0;

        %Removing lateral
        label_x=[0   size(mascara,2)-100 size(mascara,2)-60 size(mascara,2)-60  0];
        label_y=[100 100                 400                size(mascara,1)-200 size(mascara,1)-200];
        without_lateral=poly2mask(label_x,label_y,size(mascara,1),size(mascara,2));
        mascara(~without_lateral)=0;

        %Bigger area
%         kernel=ones(1500,1);
%         lines=conv2(mascara,kernel,'same')>1200;
%         mascara(lines)=0;

        max_area = extract_area(mascara,[{2} {brigther_area}]);

        image_highpass= edge(max_area,'Canny');
        
        se = strel('square', 7); 
        image_border = imdilate(image_highpass,se);

%         mask_total=sum(mask,'all')+sum(max_area,'all');
        mask_igual=sum(max_area & mask,'all');
        similarity=mask_igual/sum(max_area,'all')*100;%2*mask_igual/mask_total*100;

        images_umbral=[images_umbral;table(ini+step*i,similarity,{mask},{mascara},{max_area},{image_highpass},{image_border})];
    end  
    images_umbral.Properties.VariableNames = {'umbral','similarity','original_mask','mask','max_area','border','dil_border'};

    % Select brighter region

    [~,max_sim_idx]=max(images_umbral.similarity);
    max_sim=images_umbral(max_sim_idx,:);
        
    if show
        figure
        subplot(1,6,1),imshow(images_umbral.original_mask{max_sim_idx});
        title('Main mask')
        subplot(1,6,2),imshow(images_umbral.mask{max_sim_idx});
        title('Mask')
        subplot(1,6,3);imshow(images_umbral.max_area{max_sim_idx});
        title('Cancerous area');
        subplot(1,6,4);imshow(images_umbral.border{max_sim_idx});
        title('Border')
        subplot(1,6,5);imshow(images_umbral.dil_border{max_sim_idx});
        title('Dilated border')
        subplot(1,6,6);imshow(brigther_area);
        title('Brigter Area')
        sgtitle(sprintf('Thresholding lever %.2f. Region inside reference %0.2f %%.',images_umbral.umbral(max_sim_idx),images_umbral.similarity(max_sim_idx)))
    end
end